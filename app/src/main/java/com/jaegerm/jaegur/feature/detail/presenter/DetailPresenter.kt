/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.detail.presenter

import com.jaegerm.jaegur.common.architecture.BasePresenter
import com.jaegerm.jaegur.common.di.scope.ActivityScoped
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.data.gallery.model.imageUrl
import com.jaegerm.jaegur.feature.detail.view.DetailView
import javax.inject.Inject

interface DetailPresenter : BasePresenter<DetailView> {
    fun takeData(galleryItem: GalleryItem)
}

@ActivityScoped
class DetailPresenterImpl @Inject constructor() : DetailPresenter {

    private var detailView: DetailView? = null

    override fun takeView(view: DetailView) {
        detailView = view
    }

    override fun takeData(galleryItem: GalleryItem) {
        detailView?.showImage(galleryItem.imageUrl)
        detailView?.setTitle(galleryItem.title)
        detailView?.setUpvoteNumber(galleryItem.ups)
        detailView?.setDownvoteNumber(galleryItem.downs)
        detailView?.setScore(galleryItem.score)
        galleryItem.description?.let { detailView?.setDescription(it) }
    }

    override fun dropView() {
        detailView = null
    }
}

