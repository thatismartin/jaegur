/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.about

import com.jaegerm.jaegur.common.di.scope.ActivityScoped
import com.jaegerm.jaegur.common.di.scope.FragmentScoped
import com.jaegerm.jaegur.feature.about.presenter.AboutPresenter
import com.jaegerm.jaegur.feature.about.presenter.AboutPresenterImpl
import com.jaegerm.jaegur.feature.about.view.AboutFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AboutViewModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun aboutFragment(): AboutFragment

    @ActivityScoped
    @Binds
    abstract fun aboutPresenter(presenter: AboutPresenterImpl): AboutPresenter
}
