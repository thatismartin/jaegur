/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.gallery

import com.jaegerm.jaegur.common.di.scope.ActivityScoped
import com.jaegerm.jaegur.common.di.scope.FragmentScoped
import com.jaegerm.jaegur.feature.gallery.presenter.GalleryPresenter
import com.jaegerm.jaegur.feature.gallery.presenter.GalleryPresenterImpl
import com.jaegerm.jaegur.feature.gallery.view.HotGalleryFragment
import com.jaegerm.jaegur.feature.gallery.view.TopGalleryFragment
import com.jaegerm.jaegur.feature.gallery.view.UserGalleryFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class GalleryViewModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun hotFragment(): HotGalleryFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun topFragment(): TopGalleryFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun useFragment(): UserGalleryFragment

    @ActivityScoped
    @Binds
    abstract fun overviewPresenter(presenter: GalleryPresenterImpl): GalleryPresenter
}
