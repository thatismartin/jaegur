/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.data.gallery.local

import android.content.SharedPreferences
import com.jaegerm.jaegur.common.view.LayoutManagerType
import javax.inject.Inject

interface GalleryLocal {
    suspend fun getShowViral(): Boolean
    suspend fun setShowViral(showViral: Boolean)
    suspend fun setLayoutManagerType(layoutManagerType: LayoutManagerType)
    fun getLayoutManagerType(): LayoutManagerType
}

class GalleryLocalImpl @Inject constructor(private val sharedPreferences: SharedPreferences) : GalleryLocal {

    override fun getLayoutManagerType(): LayoutManagerType =
            sharedPreferences.getString(KEY_LAYOUT_TYPE, LayoutManagerType.GRID.toString()).let { LayoutManagerType.valueOf(it) }

    override suspend fun setLayoutManagerType(layoutManagerType: LayoutManagerType) =
            sharedPreferences.edit().putString(KEY_LAYOUT_TYPE, layoutManagerType.toString()).apply()

    override suspend fun getShowViral(): Boolean = sharedPreferences.getBoolean(KEY_SHOW_VIRAL, true)

    override suspend fun setShowViral(showViral: Boolean) = sharedPreferences.edit().putBoolean(KEY_SHOW_VIRAL, showViral).apply()

    companion object {
        private const val KEY_SHOW_VIRAL = "KEY_SHOW_VIRAL"
        private const val KEY_LAYOUT_TYPE = "KEY_LAYOUT_TYPE"
    }
}