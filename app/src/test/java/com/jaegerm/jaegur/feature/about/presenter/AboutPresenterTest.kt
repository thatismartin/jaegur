/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.about.presenter

import com.jaegerm.jaegur.feature.about.view.AboutView
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.kotlintest.specs.WordSpec

class AboutPresenterTest : WordSpec() {
    private val viewMock: AboutView = mock()
    private val presenter = AboutPresenterImpl()

    init {
        presenter.takeView(viewMock)

        "takeView" should {
            "update the development time" {
                verify(viewMock).updateDevelopmentTime()
            }

            "update the version name" {
                verify(viewMock).updateVersionName()
            }
        }

        "onContactClick" should {
            "create an email" {
                presenter.onContactClick()

                verify(viewMock).createEmail()
            }
        }
    }
}