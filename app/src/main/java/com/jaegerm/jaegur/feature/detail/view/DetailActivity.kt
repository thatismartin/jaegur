/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.detail.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.jaegerm.jaegur.R
import com.jaegerm.jaegur.common.extensions.replaceFragmentIfEmpty
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import dagger.Lazy
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class DetailActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var fragmentProvider: Lazy<DetailFragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)

        replaceFragmentIfEmpty(fragmentProvider.get())
    }

    companion object {
        const val DATA_EXTRA = "GALLERY_DATA_EXTRA"

        fun newIntent(context: Context, galleryItem: GalleryItem) =
                Intent(context, DetailActivity::class.java).apply {
                    putExtra(DATA_EXTRA, galleryItem)
                }
    }
}
