/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.data.gallery

import com.jaegerm.jaegur.common.view.LayoutManagerType
import com.jaegerm.jaegur.data.Result
import com.jaegerm.jaegur.data.gallery.local.GalleryLocal
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.data.gallery.model.enums.Section
import com.jaegerm.jaegur.data.gallery.model.enums.Sort
import com.jaegerm.jaegur.data.gallery.remote.GalleryRemote
import javax.inject.Inject

interface GalleryRepository {
    suspend fun getGallery(section: Section = Section.hot,
                           sort: Sort = Sort.viral,
                           page: Int = 0,
                           showViral: Boolean = true): Result<List<GalleryItem>>

    suspend fun getShowViralFlag(): Boolean
    suspend fun setShowViralFlag(showViral: Boolean)
    suspend fun setLayoutType(layoutManagerType: LayoutManagerType)
    suspend fun getLayoutManagerType(): LayoutManagerType
}

/**
 * The repository provides gallery data from local (not implemented, yet) and remote data sources.
 */
class GalleryRepositoryImpl @Inject constructor(private val local: GalleryLocal,
                                                private val remote: GalleryRemote) : GalleryRepository {
    override suspend fun getLayoutManagerType(): LayoutManagerType = local.getLayoutManagerType()

    override suspend fun setLayoutType(layoutManagerType: LayoutManagerType) = local.setLayoutManagerType(layoutManagerType)

    override suspend fun setShowViralFlag(showViral: Boolean) = local.setShowViral(showViral)

    override suspend fun getShowViralFlag(): Boolean = local.getShowViral()

    override suspend fun getGallery(section: Section, sort: Sort, page: Int, showViral: Boolean): Result<List<GalleryItem>> =
            remote.fetchGallery(section, sort, page, showViral)
}

