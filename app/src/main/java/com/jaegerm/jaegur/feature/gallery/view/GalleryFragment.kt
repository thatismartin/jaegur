/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.gallery.view

import android.os.Bundle
import android.os.Parcelable
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.*
import com.jaegerm.jaegur.R
import com.jaegerm.jaegur.common.view.LayoutManagerType
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.data.gallery.model.enums.Section
import com.jaegerm.jaegur.feature.detail.view.DetailActivity
import com.jaegerm.jaegur.feature.gallery.presenter.GalleryPresenter
import com.jaegerm.jaegur.feature.gallery.view.adapter.GalleryAdapter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_gallery.*
import timber.log.Timber
import javax.inject.Inject


interface GalleryView {
    fun initAdapter()
    fun updateGalleryData(galleryItems: List<GalleryItem>)
    fun showErrorView()
    fun hideErrorView()
    fun showLoadingIndicator()
    fun hideLoadingIndicator()
    fun setShowViralFlag(localShowViral: Boolean)
    fun changeToListView()
    fun changeToStaggeredGridView()
    fun changeToGridView()
    fun selectedLayoutManagerType(savedLayoutManagerType: LayoutManagerType)
}

abstract class GalleryFragment : DaggerFragment(), GalleryView {

    @Inject
    lateinit var presenter: GalleryPresenter

    abstract var section: Section

    private var showViral: Boolean = false
    private var selectedLayoutManagerType: LayoutManagerType? = null
    private var layoutManagerSavedState: Parcelable? = null

    private val imageAdapter by lazy {
        GalleryAdapter(listener = { galleryItem ->
            context?.let {
                startActivity(DetailActivity.newIntent(it, galleryItem))
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_gallery, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swipeToRefreshLayout.setOnRefreshListener { presenter.loadGallery(section) }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.action_include_viral)?.isChecked = showViral

        selectedLayoutManagerType?.let {
            val menuItem = menu.findItem(R.id.action_switch_list_type)

            when (it) {
                LayoutManagerType.STAGGERED -> updateMenuItemForStaggeredView(menuItem)
                LayoutManagerType.LIST -> updateMenuItemForListView(menuItem)
                LayoutManagerType.GRID -> updateMenuItemForGridView(menuItem)
            }
        }

        super.onPrepareOptionsMenu(menu)
    }

    override fun onResume() {
        super.onResume()
        presenter.takeView(this)
        presenter.loadGallery(section)
    }

    override fun onPause() {
        presenter.dropView()

        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(LAYOUT_MANAGER_STATE, imageRecyclerView.layoutManager?.onSaveInstanceState())
        super.onSaveInstanceState(outState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        layoutManagerSavedState = savedInstanceState?.getParcelable(LAYOUT_MANAGER_STATE)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_include_viral -> {
                handleShowViralClick(item)
                true
            }
            R.id.action_switch_list_type -> {
                handleSwitchListClick(item)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun changeToGridView() =
            updateLayoutManager(GridLayoutManager(context, 2),
                                LayoutManagerType.GRID)

    override fun changeToListView() =
            updateLayoutManager(LinearLayoutManager(context),
                                LayoutManagerType.LIST)

    override fun changeToStaggeredGridView() =
            updateLayoutManager(StaggeredGridLayoutManager(2,
                                                           StaggeredGridLayoutManager.VERTICAL),
                                LayoutManagerType.STAGGERED)

    private fun handleSwitchListClick(item: MenuItem) =
            when (item.title) {
                getString(R.string.grid_view) -> {
                    updateMenuItemForListView(item)
                    presenter.onListViewSelected()
                }
                getString(R.string.list_view) -> {

                    updateMenuItemForStaggeredView(item)
                    presenter.onStaggeredViewSelected()
                }
                getString(R.string.staggered_view) -> {
                    updateMenuItemForGridView(item)
                    presenter.onGridViewSelected()
                }
                else -> Timber.e("Unknown switch list icon")
            }

    override fun setShowViralFlag(localShowViral: Boolean) {
        showViral = localShowViral
    }

    override fun selectedLayoutManagerType(savedLayoutManagerType: LayoutManagerType) {
        selectedLayoutManagerType = savedLayoutManagerType
        activity?.invalidateOptionsMenu()
    }

    override fun initAdapter() {
        imageRecyclerView.adapter = imageAdapter
        updateLayoutManager(GridLayoutManager(context, 2), LayoutManagerType.GRID)
    }

    override fun updateGalleryData(galleryItems: List<GalleryItem>) {
        imageAdapter.data = galleryItems

        layoutManagerSavedState?.let {
            imageRecyclerView.layoutManager?.onRestoreInstanceState(it)
        }
    }

    override fun showErrorView() {
        errorView.visibility = View.VISIBLE
    }

    override fun hideErrorView() {
        errorView.visibility = View.GONE
    }

    override fun showLoadingIndicator() {
        swipeToRefreshLayout.isRefreshing = true
    }

    override fun hideLoadingIndicator() {
        swipeToRefreshLayout.isRefreshing = false
    }

    private fun updateLayoutManager(layoutManager: RecyclerView.LayoutManager, layoutManagerType: LayoutManagerType) =
            with(imageRecyclerView) {
                this.setHasFixedSize(true)
                this.layoutManager = layoutManager
                imageAdapter.layoutManagerType = layoutManagerType
            }

    private fun handleShowViralClick(item: MenuItem) {
        presenter.onShowViralClicked(item.isChecked)
        presenter.loadGallery(section = section, showViral = item.isChecked)
    }

    private fun updateMenuItemForGridView(item: MenuItem) =
            with(item) {
                setIcon(R.drawable.ic_grid)
                title = getString(R.string.grid_view)
            }

    private fun updateMenuItemForStaggeredView(item: MenuItem) =
            with(item) {
                setIcon(R.drawable.ic_staggered)
                title = getString(R.string.staggered_view)
            }

    private fun updateMenuItemForListView(item: MenuItem) =
            with(item) {
                setIcon(R.drawable.ic_list)
                title = getString(R.string.list_view)
            }

    companion object {
        private const val LAYOUT_MANAGER_STATE = "LAYOUT_MANAGER_STATE"
    }
}

