/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.data.gallery.remote

import com.jaegerm.jaegur.data.Result
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.data.gallery.model.enums.Section
import com.jaegerm.jaegur.data.gallery.model.enums.Sort
import retrofit2.HttpException
import javax.inject.Inject

interface GalleryRemote {
    suspend fun fetchGallery(section: Section, sort: Sort, page: Int, showViral: Boolean): Result<List<GalleryItem>>
}

class GalleryRemoteImpl @Inject constructor(private val galleryApi: GalleryApi) : GalleryRemote {

    override suspend fun fetchGallery(section: Section, sort: Sort, page: Int, showViral: Boolean): Result<List<GalleryItem>> =
            try {
                val result = galleryApi.getGallery(section, sort, page, showViral).await()
                Result.Success(result.data)
            } catch (e: HttpException) {
                Result.Error(e)
            } catch (e: Throwable) {
                Result.Error(e)
            }
}