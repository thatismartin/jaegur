/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.detail

import com.jaegerm.jaegur.common.di.scope.ActivityScoped
import com.jaegerm.jaegur.common.di.scope.FragmentScoped
import com.jaegerm.jaegur.feature.detail.presenter.DetailPresenter
import com.jaegerm.jaegur.feature.detail.presenter.DetailPresenterImpl
import com.jaegerm.jaegur.feature.detail.view.DetailFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DetailViewModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun detailFragment(): DetailFragment

    @ActivityScoped
    @Binds
    abstract fun detailPresenter(presenter: DetailPresenterImpl): DetailPresenter
}
