/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.common.di

import android.app.Application
import android.content.SharedPreferences
import com.jaegerm.jaegur.JaegurApp
import com.jaegerm.jaegur.common.concurrency.ConcurrencyModule
import com.jaegerm.jaegur.common.concurrency.CoroutineContextProvider
import com.jaegerm.jaegur.data.ApiModule
import com.jaegerm.jaegur.data.PreferenceModule
import com.jaegerm.jaegur.data.gallery.remote.GalleryApi
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * This is a Dagger component. Refer to {@link App} for the list of Dagger components
 * used in this application.
 * <p>
 * Even though Dagger allows annotating a {@link Component} as a singleton, the code
 * itself must ensure only one instance of the class is created. This is done in {@link
 * App}.
 */
@Singleton
@Component(modules = [
    AppModule::class,
    ActivityBindingModule::class,
    AndroidSupportInjectionModule::class,
    ApiModule::class,
    PreferenceModule::class,
    ConcurrencyModule::class])
interface AppComponent : AndroidInjector<JaegurApp> {

    fun getImgurApi(): GalleryApi

    fun getSharedPreferences(): SharedPreferences

    fun getCoroutineContextProvider(): CoroutineContextProvider

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}