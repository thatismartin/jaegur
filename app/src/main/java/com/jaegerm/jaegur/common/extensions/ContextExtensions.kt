/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.common.extensions

import android.content.Context
import android.util.TypedValue

fun Context.pxInDp(px: Int) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX,
                                                        px.toFloat(),
                                                        resources.displayMetrics).toInt()
