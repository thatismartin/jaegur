/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.detail

import com.jaegerm.jaegur.common.di.scope.ActivityScoped
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.feature.detail.view.DetailActivity
import dagger.Module
import dagger.Provides

@Module
class DetailDataModule {

    @Provides
    @ActivityScoped
    fun provideGalleryItem(activity: DetailActivity): GalleryItem = activity.intent.getParcelableExtra(DetailActivity.DATA_EXTRA)
}
