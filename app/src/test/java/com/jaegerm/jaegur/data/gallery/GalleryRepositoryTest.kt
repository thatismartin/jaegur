/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.data.gallery

import com.jaegerm.jaegur.common.view.LayoutManagerType
import com.jaegerm.jaegur.data.Result
import com.jaegerm.jaegur.data.gallery.local.GalleryLocal
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.data.gallery.model.enums.Section
import com.jaegerm.jaegur.data.gallery.model.enums.Sort
import com.jaegerm.jaegur.data.gallery.remote.GalleryRemote
import com.jaegerm.jaegur.domain.gallery.Params
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.kotlintest.shouldBe
import io.kotlintest.specs.WordSpec
import kotlinx.coroutines.experimental.runBlocking

class GalleryRepositoryTest : WordSpec() {

    private val remoteMock: GalleryRemote = mock()
    private val localMock: GalleryLocal = mock()
    private val repository = GalleryRepositoryImpl(localMock, remoteMock)

    init {
        "getGallery" should {
            "return success data when executed with parameters" {
                runBlocking {
                    whenever(remoteMock.fetchGallery(section = TEST_PARAMS.section,
                                                     sort = TEST_PARAMS.sort,
                                                     page = TEST_PARAMS.page,
                                                     showViral = TEST_PARAMS.showViral)).thenReturn(SUCCESS_DATA)

                    repository.getGallery(section = TEST_PARAMS.section,
                                          sort = TEST_PARAMS.sort,
                                          page = TEST_PARAMS.page,
                                          showViral = TEST_PARAMS.showViral) shouldBe SUCCESS_DATA
                }
            }

            "return success data when executed with default parameters" {
                runBlocking {
                    whenever(remoteMock.fetchGallery(Section.hot,
                                                     sort = Sort.viral,
                                                     page = 0,
                                                     showViral = true)).thenReturn(SUCCESS_DATA)

                    repository.getGallery() shouldBe SUCCESS_DATA
                }
            }

            "return error data when executed with parameters" {
                runBlocking {
                    whenever(remoteMock.fetchGallery(Section.hot,
                                                     sort = Sort.viral,
                                                     page = 0,
                                                     showViral = true)).thenReturn(ERROR_DATA)

                    repository.getGallery() shouldBe ERROR_DATA
                }
            }
        }

        "getShowViralFlag" should {
            runBlocking {
                whenever(localMock.getShowViral()).thenReturn(true)

                repository.getShowViralFlag() shouldBe true
            }
        }

        "getLayoutManagerType" should {
            runBlocking {
                whenever(localMock.getLayoutManagerType()).thenReturn(LayoutManagerType.GRID)

                repository.getLayoutManagerType() shouldBe LayoutManagerType.GRID
            }
        }

        "setShowViralFlag" should {
            runBlocking {
                repository.setShowViralFlag(true)
                verify(localMock).setShowViral(true)
            }
        }

        "setLayoutManagerType" should {
            runBlocking {
                repository.setLayoutType(LayoutManagerType.LIST)
                verify(localMock).setLayoutManagerType(LayoutManagerType.LIST)
            }
        }
    }

    companion object {
        val TEST_PARAMS = Params(Section.user, Sort.top, 2, true)

        val SUCCESS_DATA = Result.Success(listOf(GalleryItem(id = "lDRB2",
                                                             title = "Imgur Office",
                                                             description = null,
                                                             datetime = 1357856292,
                                                             cover = "24nLu",
                                                             accountUrl = "Alan",
                                                             accountId = 4,
                                                             privacy = "public",
                                                             layout = "blog",
                                                             views = 13780,
                                                             link = "http://alanbox.imgur.com/a/lDRB2",
                                                             ups = 1602,
                                                             downs = 14,
                                                             points = 1588,
                                                             score = 1917,
                                                             isAlbum = true,
                                                             vote = null,
                                                             commentCount = 10,
                                                             imagesCount = 11,
                                                             images = emptyList(),
                                                             animated = false,
                                                             section = "section",
                                                             bandwidth = 123L,
                                                             height = 123,
                                                             size = 123,
                                                             type = "type",
                                                             width = 123)))

        val ERROR_DATA = Result.Error(IllegalArgumentException("Some error"))
    }
}