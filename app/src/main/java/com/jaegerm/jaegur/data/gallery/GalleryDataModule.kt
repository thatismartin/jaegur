/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.data.gallery

import com.jaegerm.jaegur.common.di.scope.ActivityScoped
import com.jaegerm.jaegur.data.gallery.local.GalleryLocal
import com.jaegerm.jaegur.data.gallery.local.GalleryLocalImpl
import com.jaegerm.jaegur.data.gallery.remote.GalleryRemote
import com.jaegerm.jaegur.data.gallery.remote.GalleryRemoteImpl
import dagger.Binds
import dagger.Module

/**
 * The gallery data module provides all available sources to work with galleries.
 */
@Module
abstract class GalleryDataModule {

    @ActivityScoped
    @Binds
    abstract fun provideRemoteSource(remote: GalleryRemoteImpl): GalleryRemote

    @ActivityScoped
    @Binds
    abstract fun provideLocalSource(local: GalleryLocalImpl): GalleryLocal

    @ActivityScoped
    @Binds
    abstract fun provideRepository(repository: GalleryRepositoryImpl): GalleryRepository
}