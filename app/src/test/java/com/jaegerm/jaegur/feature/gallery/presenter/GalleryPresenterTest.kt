/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.gallery.presenter

import com.jaegerm.jaegur.common.concurrency.TestContextProvider
import com.jaegerm.jaegur.data.Result
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.domain.gallery.*
import com.jaegerm.jaegur.feature.gallery.view.GalleryView
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.kotlintest.specs.WordSpec
import kotlinx.coroutines.experimental.runBlocking

class GalleryPresenterTest : WordSpec() {

    private val viewMock: GalleryView = mock()
    private val getGalleryImagesUseCase: GetGalleryImagesUseCase = mock()
    private val getShowViralFlagUseCase: GetShowViralFlagUseCase = mock()
    private val setShowViralFlagUseCase: SetShowViralFlagUseCase = mock()
    private val getGalleryTypeUseCase: GetGalleryTypeUseCase = mock()
    private val setGalleryTypeUseCase: SetGalleryTypeUseCase = mock()

    private val presenter = GalleryPresenterImpl(getGalleryImagesUseCase,
                                                 getShowViralFlagUseCase,
                                                 setShowViralFlagUseCase,
                                                 setGalleryTypeUseCase,
                                                 getGalleryTypeUseCase,
                                                 TestContextProvider())

    init {
        "takeView" should {
            "update the data when usecase returns with success" {
                runBlocking {
                    presenter.takeView(viewMock)
                    verify(viewMock).initAdapter()
                }
            }
        }
    }

    companion object {
        private val TEST_GALLERY_ITEM_RESULT = Result.Success(listOf(GalleryItem(id = "lDRB2",
                                                                                 title = "Imgur Office",
                                                                                 description = null,
                                                                                 datetime = 1357856292,
                                                                                 cover = "24nLu",
                                                                                 accountUrl = "Alan",
                                                                                 accountId = 4,
                                                                                 privacy = "public",
                                                                                 layout = "blog",
                                                                                 views = 13780,
                                                                                 link = "http://alanbox.imgur.com/a/lDRB2",
                                                                                 ups = 1602,
                                                                                 downs = 14,
                                                                                 points = 1588,
                                                                                 score = 1917,
                                                                                 isAlbum = true,
                                                                                 vote = null,
                                                                                 commentCount = 10,
                                                                                 imagesCount = 11,
                                                                                 images = emptyList(),
                                                                                 animated = false,
                                                                                 section = "section",
                                                                                 bandwidth = 123L,
                                                                                 height = 123,
                                                                                 size = 123,
                                                                                 type = "image/png",
                                                                                 width = 123)))

        val ERROR_DATA = Result.Error(IllegalArgumentException("Some error"))
    }
}