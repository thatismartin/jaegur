/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.about.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.jaegerm.jaegur.R
import com.jaegerm.jaegur.common.extensions.replaceFragmentIfEmpty
import dagger.Lazy
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class AboutActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var fragmentProvider: Lazy<AboutFragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)

        replaceFragmentIfEmpty(fragmentProvider.get())
    }

    companion object {
        fun newIntent(context: Context) = Intent(context, AboutActivity::class.java)
    }
}
