/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.detail.view

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.jaegerm.jaegur.R
import com.jaegerm.jaegur.common.view.GlideApp
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.feature.detail.presenter.DetailPresenter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_detail.*
import javax.inject.Inject

interface DetailView {
    fun showImage(imageUrl: String?)
    fun setTitle(title: String)
    fun setDescription(description: String)
    fun setUpvoteNumber(ups: Int)
    fun setDownvoteNumber(downs: Int)
    fun setScore(score: Int)
}

class DetailFragment @Inject constructor() : DaggerFragment(), DetailView {

    @Inject
    lateinit var presenter: DetailPresenter

    @Inject
    lateinit var galleryItem: GalleryItem

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_detail, container, false)

    override fun onResume() {
        super.onResume()
        presenter.takeView(this)
        presenter.takeData(galleryItem)
    }

    override fun onPause() {
        presenter.dropView()
        super.onPause()
    }

    override fun showImage(imageUrl: String?) {
        val glideBuilder = GlideApp
                .with(this)
                .load(imageUrl)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        loadingAnimationView.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        loadingAnimationView.visibility = View.GONE
                        return false
                    }
                })

        glideBuilder.into(imageView)
    }

    override fun setTitle(title: String) {
        titleView.text = title
    }

    override fun setDescription(description: String) {
        descriptionView.text = description
        descriptionView.visibility = View.VISIBLE
    }

    override fun setUpvoteNumber(ups: Int) {
        upvoteNumberView.text = ups.toString()
    }

    override fun setDownvoteNumber(downs: Int) {
        downvoteNumberView.text = downs.toString()
    }

    override fun setScore(score: Int) {
        scoreNumberView.text = score.toString()
    }
}
