/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.data.gallery.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class GalleryItem(@SerializedName("id") val id: String,
                       @SerializedName("title") val title: String,
                       @SerializedName("description") val description: String?,
                       @SerializedName("datetime") val datetime: Int,
                       @SerializedName("cover") val cover: String?,
                       @SerializedName("account_url") val accountUrl: String,
                       @SerializedName("account_id") val accountId: Int,
                       @SerializedName("privacy") val privacy: String,
                       @SerializedName("layout") val layout: String,
                       @SerializedName("views") val views: Int,
                       @SerializedName("link") val link: String,
                       @SerializedName("ups") val ups: Int,
                       @SerializedName("downs") val downs: Int,
                       @SerializedName("points") val points: Int,
                       @SerializedName("score") val score: Int,
                       @SerializedName("is_album") val isAlbum: Boolean,
                       @SerializedName("vote") val vote: String?,
                       @SerializedName("comment_count") val commentCount: Int,
                       @SerializedName("images_count") val imagesCount: Int,
                       @SerializedName("type") val type: String?,
                       @SerializedName("animated") val animated: Boolean?,
                       @SerializedName("width") val width: Int?,
                       @SerializedName("height") val height: Int?,
                       @SerializedName("size") val size: Int?,
                       @SerializedName("bandwidth") val bandwidth: Long?,
                       @SerializedName("section") val section: String?,
                       @SerializedName("images") val images: List<Image>?) : Parcelable {

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            1 == source.readInt(),
            source.readString(),
            source.readInt(),
            source.readInt(),
            source.readString(),
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readString(),
            source.createTypedArrayList(Image.CREATOR)
                                      )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(title)
        writeString(description)
        writeInt(datetime)
        writeString(cover)
        writeString(accountUrl)
        writeInt(accountId)
        writeString(privacy)
        writeString(layout)
        writeInt(views)
        writeString(link)
        writeInt(ups)
        writeInt(downs)
        writeInt(points)
        writeInt(score)
        writeInt((if (isAlbum) 1 else 0))
        writeString(vote)
        writeInt(commentCount)
        writeInt(imagesCount)
        writeString(type)
        writeValue(animated)
        writeValue(width)
        writeValue(height)
        writeValue(size)
        writeValue(bandwidth)
        writeString(section)
        writeTypedList(images)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<GalleryItem> = object : Parcelable.Creator<GalleryItem> {
            override fun createFromParcel(source: Parcel): GalleryItem = GalleryItem(source)
            override fun newArray(size: Int): Array<GalleryItem?> = arrayOfNulls(size)
        }
    }
}

val GalleryItem.imageUrl: String?
    get() = images?.firstOrNull()?.link ?: link
