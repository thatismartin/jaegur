/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.data

import android.content.Context
import com.jaegerm.jaegur.BuildConfig
import com.jaegerm.jaegur.data.gallery.remote.GalleryApi
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * The api module defines the API configuration and provides all REST related objects
 * like a http client, the retrofit service, caching...
 */
@Module
class ApiModule {

    class ImgurHeaderInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            chain.request().let { request ->
                // re-write response header to force use of cache
                val cacheControl = CacheControl.Builder()
                        .maxAge(2, TimeUnit.MINUTES)
                        .build()

                val modifiedRequest =
                        request.newBuilder()
                                .addHeader("Cache-Control", cacheControl.toString())
                                .addHeader("Authorization", "Client-ID $API_KEY")
                                .build()

                return chain.proceed(modifiedRequest)
            }
        }
    }

    @Singleton
    @Provides
    fun provideRetrofit(httpClient: OkHttpClient): Retrofit =
            Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .build()

    @Singleton
    @Provides
    fun provideImgurApi(retrofit: Retrofit): GalleryApi = retrofit.create(GalleryApi::class.java)

    @Singleton
    @Provides
    fun provideHttpClient(cache: Cache,
                          headerInterceptor: ImgurHeaderInterceptor,
                          loggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .cache(cache)
                    .addInterceptor(headerInterceptor)
                    .addInterceptor(loggingInterceptor)
                    .build()

    @Singleton
    @Provides
    fun provideImgurHeaderInterceptor(): ImgurHeaderInterceptor = ImgurHeaderInterceptor()

    @Singleton
    @Provides
    fun provideHttpLoggerInterceptor(): HttpLoggingInterceptor =
            HttpLoggingInterceptor().apply {
                var logLevel = HttpLoggingInterceptor.Level.NONE

                if (BuildConfig.DEBUG) logLevel = HttpLoggingInterceptor.Level.HEADERS

                this.level = logLevel
            }

    @Singleton
    @Provides
    fun provideCache(context: Context): Cache = Cache(context.cacheDir, cacheSize)

    companion object {
        private const val BASE_URL = "https://api.imgur.com/3/"
        private const val API_KEY = "e1f1888dbe4bb1a"

        private val cacheSize: Long = 25 * 1024 * 1024 // 25 MB
    }
}