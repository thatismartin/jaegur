/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.common.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import com.jaegerm.jaegur.R

class ErrorView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        ConstraintLayout.inflate(context, R.layout.custom_error_view, this)
        layoutParams = ConstraintLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)

        setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
    }
}