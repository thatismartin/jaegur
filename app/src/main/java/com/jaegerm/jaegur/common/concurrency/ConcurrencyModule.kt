/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.common.concurrency

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ConcurrencyModule {

    @Singleton
    @Provides
    fun provideCoroutineContext(): CoroutineContextProvider = CoroutineContextProvider()
}