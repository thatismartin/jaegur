/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.common.extensions

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.jaegerm.jaegur.R
import dagger.android.support.DaggerAppCompatActivity

@IdRes
const val FRAGMENT_CONTAINER_ID: Int = R.id.fragmentContainer

fun DaggerAppCompatActivity.replaceFragmentIfEmpty(fragment: Fragment, tag: String? = null) {
    if (findFragment() == null) {
        replaceFragment(fragment = fragment,
                        addToBackStack = false,
                        tag = tag)
    }
}

private fun AppCompatActivity.findFragment(): Fragment? = supportFragmentManager.findFragmentById(FRAGMENT_CONTAINER_ID)

fun AppCompatActivity.replaceFragment(fragment: Fragment, addToBackStack: Boolean = false, tag: String? = null) =
        supportFragmentManager.beginTransaction().apply {
            replace(FRAGMENT_CONTAINER_ID, fragment, tag)
            if (addToBackStack) {
                addToBackStack(null)
            }
        }.commit()


