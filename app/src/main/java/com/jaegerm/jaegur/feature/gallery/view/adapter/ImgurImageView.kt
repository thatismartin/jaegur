/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.gallery.view.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.jaegerm.jaegur.R
import com.jaegerm.jaegur.common.extensions.pxInDp
import com.jaegerm.jaegur.common.view.GlideApp
import com.jaegerm.jaegur.common.view.GlideOptions.bitmapTransform
import com.jaegerm.jaegur.common.view.LayoutManagerType
import jp.wasabeef.glide.transformations.CropTransformation
import kotlinx.android.synthetic.main.adapter_imgur_image_item.view.*

class ImgurImageView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : CardView(context, attrs, defStyleAttr) {

    private var itemClickListener: (() -> Unit)? = null
    private var layoutManagerType: LayoutManagerType? = null

    init {
        CardView.inflate(context, R.layout.adapter_imgur_image_item, this)

        with(context) {
            layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
                setMargins(pxInDp(20), pxInDp(15), pxInDp(20), pxInDp(15))
                radius = 30f
                minimumHeight = 300
            }
        }

        imgurItemContainer.setOnClickListener { itemClickListener?.invoke() }
    }

    fun setTitle(title: String) {
        textView.text = title
        imageView.contentDescription = "${context.getString(R.string.image_content_description)} $title"
    }

    fun setImage(imageUrl: String) {
        val glideBuilder = GlideApp
                .with(context)
                .load(imageUrl)

        when (layoutManagerType) {
            LayoutManagerType.LIST -> glideBuilder.centerInside()
            LayoutManagerType.GRID -> glideBuilder.apply(bitmapTransform(CropTransformation(300, 300, CropTransformation.CropType.TOP)))
        }

        glideBuilder.into(imageView)
    }

    fun setItemClickListener(function: () -> Unit) {
        itemClickListener = function
    }


    fun setLayoutManagerType(layoutType: LayoutManagerType?) {
        layoutManagerType = layoutType
    }
}
