/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.data.gallery.model

import com.google.gson.annotations.SerializedName

data class GalleryResponse(@SerializedName("data") val data: List<GalleryItem>,
                           @SerializedName("success") val success: Boolean,
                           @SerializedName("status") val status: Int)