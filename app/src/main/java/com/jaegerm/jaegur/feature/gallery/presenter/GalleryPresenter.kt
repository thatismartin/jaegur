/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.gallery.presenter

import com.jaegerm.jaegur.common.architecture.BasePresenter
import com.jaegerm.jaegur.common.concurrency.CoroutineContextProvider
import com.jaegerm.jaegur.common.di.scope.ActivityScoped
import com.jaegerm.jaegur.common.view.LayoutManagerType
import com.jaegerm.jaegur.data.Result
import com.jaegerm.jaegur.data.gallery.model.enums.Section
import com.jaegerm.jaegur.domain.gallery.*
import com.jaegerm.jaegur.feature.gallery.view.GalleryView
import kotlinx.coroutines.experimental.launch
import timber.log.Timber
import javax.inject.Inject

interface GalleryPresenter : BasePresenter<GalleryView> {
    fun loadGallery(section: Section, showViral: Boolean? = null)
    fun onShowViralClicked(selected: Boolean)
    fun onListViewSelected()
    fun onStaggeredViewSelected()
    fun onGridViewSelected()
}

@ActivityScoped
class GalleryPresenterImpl @Inject constructor(private val getGalleryUseCase: GetGalleryImagesUseCase,
                                               private val getShowViralFlagUseCase: GetShowViralFlagUseCase,
                                               private val setShowViralFlagUseCase: SetShowViralFlagUseCase,
                                               private val setGalleryLayoutTypeUseCase: SetGalleryTypeUseCase,
                                               private val getGalleryLayoutTypeUseCase: GetGalleryTypeUseCase,
                                               private val contextPool: CoroutineContextProvider) : GalleryPresenter {

    private var overviewView: GalleryView? = null

    override fun takeView(view: GalleryView) {
        overviewView = view
        overviewView?.initAdapter()

        launch(contextPool.IO) {
            val showViral = getShowViralFlagUseCase.execute()
            val savedLayoutManagerType = getGalleryLayoutTypeUseCase.execute()

            launch(contextPool.Main) {
                overviewView?.setShowViralFlag(showViral)
                overviewView?.selectedLayoutManagerType(savedLayoutManagerType)

                when (savedLayoutManagerType) {
                    LayoutManagerType.GRID -> overviewView?.changeToGridView()
                    LayoutManagerType.LIST -> overviewView?.changeToListView()
                    LayoutManagerType.STAGGERED -> overviewView?.changeToStaggeredGridView()
                }
            }
        }
    }

    override fun loadGallery(section: Section, showViral: Boolean?) {
        showLoadingIndicator()

        launch(contextPool.IO) {
            val params = buildRequestParams(section, showViral ?: getShowViralFlagUseCase.execute())
            val result = getGalleryUseCase.execute(params)

            when (result) {
                is Result.Success -> {
                    launch(contextPool.Main) {
                        hideLoadingIndicator()
                        overviewView?.hideErrorView()
                        overviewView?.updateGalleryData(result.data)
                    }
                }
                is Result.Error -> {
                    launch(contextPool.Main) {
                        Timber.e(result.exception, "Failed to load initial gallery!")

                        hideLoadingIndicator()
                        overviewView?.showErrorView()
                    }
                }
            }
        }
    }

    override fun onShowViralClicked(selected: Boolean) {
        launch(contextPool.IO) {
            setShowViralFlagUseCase.execute(SetShowViralFlagUseCase.Params(selected))

            launch(contextPool.Main) {
                overviewView?.setShowViralFlag(selected)
            }
        }
    }

    override fun dropView() {
        overviewView = null
    }

    override fun onListViewSelected() {
        launch {
            setGalleryLayoutTypeUseCase.execute(SetGalleryTypeUseCase.Params(LayoutManagerType.LIST))
        }
        overviewView?.changeToListView()
    }

    override fun onStaggeredViewSelected() {
        launch {
            setGalleryLayoutTypeUseCase.execute(SetGalleryTypeUseCase.Params(LayoutManagerType.STAGGERED))
        }
        overviewView?.changeToStaggeredGridView()
    }

    override fun onGridViewSelected() {
        launch {
            setGalleryLayoutTypeUseCase.execute(SetGalleryTypeUseCase.Params(LayoutManagerType.GRID))
        }
        overviewView?.changeToGridView()
    }

    private fun buildRequestParams(section: Section, showViral: Boolean?) = Params().apply {
        this.section = section
        showViral?.let { nonNullShowViral -> this.showViral = nonNullShowViral }
    }

    private fun hideLoadingIndicator() = overviewView?.hideLoadingIndicator()

    private fun showLoadingIndicator() = overviewView?.showLoadingIndicator()
}
