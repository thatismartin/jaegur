/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.common.architecture

/**
 * A UseCase with no parameter arguments
 * @param <R> return type which will be returned by the UseCase
 *
 * @see UseCase
 */
interface NonArgUseCase<out R> {
    /**
     * Executes the use case.
     * @return <R> - instance of result.
     */
    suspend fun execute(): R
}