/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.common.di

import com.jaegerm.jaegur.common.di.scope.ActivityScoped
import com.jaegerm.jaegur.data.gallery.GalleryRepository
import com.jaegerm.jaegur.domain.gallery.GetGalleryImagesUseCase
import dagger.Module
import dagger.Provides

@Module
class GalleryUseCaseModule {

    @ActivityScoped
    @Provides
    fun provideUseCase(repository: GalleryRepository): GetGalleryImagesUseCase = GetGalleryImagesUseCase(repository)
}
