/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.domain.gallery

import com.jaegerm.jaegur.common.architecture.UseCase
import com.jaegerm.jaegur.data.Result
import com.jaegerm.jaegur.data.gallery.GalleryRepository
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.data.gallery.model.enums.Section
import com.jaegerm.jaegur.data.gallery.model.enums.Sort
import javax.inject.Inject

data class Params(var section: Section = Section.hot,
                  var sort: Sort = Sort.viral,
                  var page: Int = 0,
                  var showViral: Boolean = true)

/**
 * Returns a list with imgur gallery items. Currently, excludes gifs due to performance and networking issues (some gif urls do not use https).
 */
class GetGalleryImagesUseCase @Inject constructor(private val repository: GalleryRepository) : UseCase<Params?, Result<List<GalleryItem>>> {

    override suspend fun execute(params: Params?): Result<List<GalleryItem>> {
        val galleryItemResult = params?.let {

            repository.getGallery(section = it.section,
                                  sort = it.sort,
                                  page = it.page,
                                  showViral = it.showViral)
        } ?: repository.getGallery()

        return if (galleryItemResult is Result.Success<List<GalleryItem>>) {
            // A filtered list of gallery albums without gifs.
            // A gallery album has no type on root level but children with types.
            val filteredGalleryAlbum =
                    galleryItemResult
                            .data
                            .filter { galleryItem -> galleryItem.type.isNullOrEmpty() && galleryItem.images?.any { isNotGif(it.type) } ?: false }

            // A filtered list of gallery images without gifs.
            // A gallery image can be identified by a set type on root level.
            val filteredGalleryImages =
                    galleryItemResult
                            .data
                            .filter { galleryItem ->
                                galleryItem.type?.let { isNotGif(it) } ?: false
                            }

            val joinedList = ArrayList<GalleryItem>()

            joinedList.addAll(filteredGalleryAlbum)
            joinedList.addAll(filteredGalleryImages)

            return Result.Success(joinedList)

        } else galleryItemResult
    }

    private fun isNotGif(type: String): Boolean = type.equals(MIME_TYPE_GIF).not()

    companion object {
        private const val MIME_TYPE_GIF = "image/gif"
    }
}