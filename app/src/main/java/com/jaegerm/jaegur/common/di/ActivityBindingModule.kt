/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.common.di

import com.jaegerm.jaegur.common.di.scope.ActivityScoped
import com.jaegerm.jaegur.data.gallery.GalleryDataModule
import com.jaegerm.jaegur.feature.about.AboutViewModule
import com.jaegerm.jaegur.feature.about.view.AboutActivity
import com.jaegerm.jaegur.feature.detail.DetailDataModule
import com.jaegerm.jaegur.feature.detail.DetailViewModule
import com.jaegerm.jaegur.feature.detail.view.DetailActivity
import com.jaegerm.jaegur.feature.gallery.GalleryViewModule
import com.jaegerm.jaegur.feature.gallery.view.GalleryActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [GalleryViewModule::class, GalleryDataModule::class, GalleryUseCaseModule::class])
    abstract fun galleryActivity(): GalleryActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [AboutViewModule::class])
    abstract fun aboutActivity(): AboutActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [DetailViewModule::class, DetailDataModule::class])
    abstract fun detailActivity(): DetailActivity
}
