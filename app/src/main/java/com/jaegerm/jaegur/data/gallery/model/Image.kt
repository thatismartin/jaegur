/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.data.gallery.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Image(@SerializedName("id") val id: String,
                 @SerializedName("title") val title: String?,
                 @SerializedName("description") val description: String?,
                 @SerializedName("datetime") val datetime: Int,
                 @SerializedName("type") val type: String,
                 @SerializedName("animated") val animated: Boolean,
                 @SerializedName("width") val width: Int,
                 @SerializedName("height") val height: Int,
                 @SerializedName("size") val size: Int,
                 @SerializedName("views") val views: Int,
                 @SerializedName("bandwidth") val bandwidth: Long,
                 @SerializedName("link") val link: String) : Parcelable {

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString(),
            1 == source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readLong(),
            source.readString()
                                      )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(title)
        writeString(description)
        writeInt(datetime)
        writeString(type)
        writeInt((if (animated) 1 else 0))
        writeInt(width)
        writeInt(height)
        writeInt(size)
        writeInt(views)
        writeLong(bandwidth)
        writeString(link)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Image> = object : Parcelable.Creator<Image> {
            override fun createFromParcel(source: Parcel): Image = Image(source)
            override fun newArray(size: Int): Array<Image?> = arrayOfNulls(size)
        }
    }
}