/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.common.architecture

/**
 * A UseCase executes a business case.
 * @param <P> parameter class which is consumed by the UseCase
 * @param <R> return type which will be returned by the UseCase
 */
interface UseCase<in P, out R> {

    /**
     * Executes the use case.
     * @param params <P> optional param for this use case execution.
     * @return <R> the type of result.
     */
    suspend fun execute(params: P): R
}