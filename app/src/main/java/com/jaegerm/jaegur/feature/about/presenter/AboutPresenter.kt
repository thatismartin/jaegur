/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.about.presenter

import com.jaegerm.jaegur.common.architecture.BasePresenter
import com.jaegerm.jaegur.common.di.scope.ActivityScoped
import com.jaegerm.jaegur.feature.about.view.AboutView
import javax.inject.Inject

interface AboutPresenter : BasePresenter<AboutView> {
    fun onContactClick()
}

@ActivityScoped
class AboutPresenterImpl @Inject constructor() : AboutPresenter {
    private var aboutView: AboutView? = null

    override fun takeView(view: AboutView) {
        aboutView = view

        aboutView?.updateDevelopmentTime()
        aboutView?.updateVersionName()
    }

    override fun onContactClick() {
        aboutView?.createEmail()
    }

    override fun dropView() {
        aboutView = null
    }
}

