/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.gallery.view

import com.jaegerm.jaegur.common.di.scope.ActivityScoped
import com.jaegerm.jaegur.data.gallery.model.enums.Section
import com.jaegerm.jaegur.feature.gallery.view.GalleryFragment
import javax.inject.Inject

@ActivityScoped
class HotGalleryFragment @Inject constructor() : GalleryFragment() {
    override var section: Section = Section.hot
}

