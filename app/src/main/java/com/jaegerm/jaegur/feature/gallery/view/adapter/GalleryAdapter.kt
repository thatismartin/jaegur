/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.gallery.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.jaegerm.jaegur.common.view.LayoutManagerType
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.data.gallery.model.imageUrl
import kotlin.properties.Delegates

typealias OnItemSelectedListener = (item: GalleryItem) -> Unit

class GalleryAdapter(val listener: OnItemSelectedListener) : RecyclerView.Adapter<GalleryViewHolder>() {

    var data: List<GalleryItem> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    var layoutManagerType: LayoutManagerType? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder =
            GalleryViewHolder(ImgurImageView(parent.context))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) =
            holder.bind(data[position], listener, layoutManagerType)
}

class GalleryViewHolder(val imageView: ImgurImageView) : RecyclerView.ViewHolder(imageView) {
    fun bind(item: GalleryItem, listener: OnItemSelectedListener, layoutManagerType: LayoutManagerType?) {
        with(imageView) {
            setLayoutManagerType(layoutManagerType)
            setTitle(item.title)
            setItemClickListener { listener.invoke(item) }
            item.imageUrl?.let { setImage(it) }
        }
    }
}