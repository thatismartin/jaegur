/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.data.gallery.remote

import com.jaegerm.jaegur.data.gallery.model.GalleryResponse
import com.jaegerm.jaegur.data.gallery.model.enums.Section
import com.jaegerm.jaegur.data.gallery.model.enums.Sort
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GalleryApi {

    @GET("gallery/{section}/{sort}/{page}")
    fun getGallery(@Path("section") section: Section,
                   @Path("sort") sort: Sort,
                   @Path("page") page: Int,
                   @Query("showViral") showViral: Boolean): Deferred<GalleryResponse>

}