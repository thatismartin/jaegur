/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.gallery.view

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.jaegerm.jaegur.R
import com.jaegerm.jaegur.common.extensions.replaceFragment
import com.jaegerm.jaegur.common.extensions.replaceFragmentIfEmpty
import com.jaegerm.jaegur.feature.about.view.AboutActivity
import dagger.Lazy
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_gallery.*
import javax.inject.Inject

class GalleryActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var hotProvider: Lazy<HotGalleryFragment>

    @Inject
    lateinit var topProvider: Lazy<TopGalleryFragment>

    @Inject
    lateinit var userProvider: Lazy<UserGalleryFragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)

        initListener()

        setSupportActionBar(toolbar)

        replaceFragmentIfEmpty(hotProvider.get())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_about -> startActivity(AboutActivity.newIntent(this))
            R.id.action_include_viral -> item.isChecked = item.isChecked.not()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initListener() {
        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_hot -> replaceFragment(hotProvider.get())
                R.id.action_trending -> replaceFragment(topProvider.get())
                R.id.action_community -> replaceFragment(userProvider.get())
            }
            true
        }
    }
}
