/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.domain.gallery

import com.jaegerm.jaegur.common.architecture.NonArgUseCase
import com.jaegerm.jaegur.common.view.LayoutManagerType
import com.jaegerm.jaegur.data.gallery.GalleryRepository
import javax.inject.Inject

class GetGalleryTypeUseCase @Inject constructor(private val repository: GalleryRepository) : NonArgUseCase<LayoutManagerType> {
    override suspend fun execute(): LayoutManagerType = repository.getLayoutManagerType()
}