/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.feature.about.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jaegerm.jaegur.BuildConfig
import com.jaegerm.jaegur.R
import com.jaegerm.jaegur.feature.about.presenter.AboutPresenter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_about.*
import timber.log.Timber
import javax.inject.Inject

interface AboutView {
    fun updateDevelopmentTime()
    fun updateVersionName()
    fun createEmail()
}

class AboutFragment @Inject constructor() : DaggerFragment(), AboutView {

    @Inject
    lateinit var presenter: AboutPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_about, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        contactButton.setOnClickListener { presenter.onContactClick() }
    }

    override fun onResume() {
        super.onResume()
        presenter.takeView(this)
    }

    override fun onPause() {
        presenter.dropView()
        super.onPause()
    }

    override fun updateDevelopmentTime() {
        devTimeView.text = DEV_TIME
    }

    override fun updateVersionName() {
        versionView.text = BuildConfig.VERSION_NAME
    }

    override fun createEmail() {
        Intent(Intent.ACTION_SENDTO).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " - " + getString(R.string.feedback))
            data = Uri.parse("mailto:${getString(R.string.email)}")
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            activity?.packageManager?.let {
                if (this.resolveActivity(it) != null) {
                    startActivity(this)
                } else if (BuildConfig.DEBUG) {
                    Timber.e("E-Mail intent can't be handled.")
                }
            }
        }
    }

    companion object {
        private const val DEV_TIME = "27h"
    }
}

