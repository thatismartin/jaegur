/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.data.gallery.remote

import com.jaegerm.jaegur.data.Result
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.data.gallery.model.GalleryResponse
import com.jaegerm.jaegur.data.gallery.model.enums.Section
import com.jaegerm.jaegur.data.gallery.model.enums.Sort
import com.jaegerm.jaegur.domain.gallery.Params
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.kotlintest.shouldBe
import io.kotlintest.specs.WordSpec
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking

class GalleryRemoteTest : WordSpec() {

    private val apiMock: GalleryApi = mock()
    private val remote = GalleryRemoteImpl(apiMock)

    init {
        "getGallery" should {
            "return success data when executed with parameters" {
                runBlocking {
                    whenever(apiMock.getGallery(section = TEST_PARAMS.section,
                                                sort = TEST_PARAMS.sort,
                                                page = TEST_PARAMS.page,
                                                showViral = TEST_PARAMS.showViral)).thenReturn(RESULT_DATA)

                    val result: Result.Success<List<GalleryItem>> = remote.fetchGallery(section = TEST_PARAMS.section,
                                                                                        sort = TEST_PARAMS.sort,
                                                                                        page = TEST_PARAMS.page,
                                                                                        showViral = TEST_PARAMS.showViral) as Result.Success<List<GalleryItem>>

                    result.data shouldBe TEST_GALLERY_ITEMS
                }
            }
        }
    }

    companion object {
        private val TEST_GALLERY_ITEMS = listOf(GalleryItem(id = "lDRB2",
                                                            title = "Imgur Office",
                                                            description = null,
                                                            datetime = 1357856292,
                                                            cover = "24nLu",
                                                            accountUrl = "Alan",
                                                            accountId = 4,
                                                            privacy = "public",
                                                            layout = "blog",
                                                            views = 13780,
                                                            link = "http://alanbox.imgur.com/a/lDRB2",
                                                            ups = 1602,
                                                            downs = 14,
                                                            points = 1588,
                                                            score = 1917,
                                                            isAlbum = true,
                                                            vote = null,
                                                            commentCount = 10,
                                                            imagesCount = 11,
                                                            images = emptyList(),
                                                            animated = false,
                                                            section = "section",
                                                            bandwidth = 123L,
                                                            height = 123,
                                                            size = 123,
                                                            type = "type",
                                                            width = 123))

        val TEST_GALLERY = GalleryResponse(data = TEST_GALLERY_ITEMS, success = true, status = 200)
        val TEST_PARAMS = Params(Section.user, Sort.top, 2, true)
        val RESULT_DATA = async { TEST_GALLERY }
    }
}