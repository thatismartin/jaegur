/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.domain.gallery

import com.jaegerm.jaegur.common.architecture.UseCase
import com.jaegerm.jaegur.data.gallery.GalleryRepository
import javax.inject.Inject

class SetShowViralFlagUseCase @Inject constructor(private val repository: GalleryRepository) : UseCase<SetShowViralFlagUseCase.Params, Unit> {
    data class Params(var showViral: Boolean)

    override suspend fun execute(params: Params): Unit = repository.setShowViralFlag(params.showViral)
}
