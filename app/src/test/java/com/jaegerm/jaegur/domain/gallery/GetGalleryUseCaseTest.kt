/*
 * Copyright (C) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Martin Jäger <thatismartin@gmail.com>, 2018
 */

package com.jaegerm.jaegur.domain.gallery

import com.jaegerm.jaegur.data.Result
import com.jaegerm.jaegur.data.gallery.GalleryRepository
import com.jaegerm.jaegur.data.gallery.model.GalleryItem
import com.jaegerm.jaegur.data.gallery.model.enums.Section
import com.jaegerm.jaegur.data.gallery.model.enums.Sort
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.kotlintest.shouldBe
import io.kotlintest.specs.WordSpec
import kotlinx.coroutines.experimental.runBlocking

class GetGalleryUseCaseTest : WordSpec() {

    private val repositoryMock: GalleryRepository = mock()
    private val useCase = GetGalleryImagesUseCase(repositoryMock)

    init {
        "execute" should {
            "return success data when executed without parameters" {
                runBlocking {
                    val expected = Result.Success(listOf(JPG_ITEM))

                    whenever(repositoryMock.getGallery()).thenReturn(expected)

                    val data = useCase.execute(null)

                    (data as Result.Success<List<GalleryItem>>).data shouldBe expected.data
                }
            }

            "return two items when both returning items are images" {
                runBlocking {
                    val expected = Result.Success(listOf(JPG_ITEM, PNG_ITEM))
                    whenever(repositoryMock.getGallery()).thenReturn(expected)

                    val data = useCase.execute(null)

                    (data as Result.Success<List<GalleryItem>>).data shouldBe expected.data
                }
            }

            "return one item when returning list has mixed types" {
                runBlocking {
                    val expected = Result.Success(listOf(JPG_ITEM, PNG_ITEM))
                    whenever(repositoryMock.getGallery()).thenReturn(expected)

                    val data = useCase.execute(null)

                    (data as Result.Success<List<GalleryItem>>).data shouldBe expected.data
                }
            }

            "return zero items when returning items are only videos" {
                runBlocking {
                    val expected = Result.Success(listOf(GIF_ITEM, GIF_ITEM))
                    whenever(repositoryMock.getGallery()).thenReturn(expected)

                    val data = useCase.execute(null)

                    (data as Result.Success<List<GalleryItem>>).data shouldBe emptyList()
                }
            }

            "return success data when executed with parameters" {
                runBlocking {
                    val expected = Result.Success(listOf(JPG_ITEM))

                    whenever(repositoryMock.getGallery(section = TEST_PARAMS.section,
                                                       sort = TEST_PARAMS.sort,
                                                       page = TEST_PARAMS.page,
                                                       showViral = TEST_PARAMS.showViral)).thenReturn(expected)

                    val data = useCase.execute(TEST_PARAMS)

                    (data as Result.Success<List<GalleryItem>>).data shouldBe expected.data
                }
            }

            "return error data when executed with parameters" {
                runBlocking {
                    whenever(repositoryMock.getGallery(section = TEST_PARAMS.section,
                                                       sort = TEST_PARAMS.sort,
                                                       page = TEST_PARAMS.page,
                                                       showViral = TEST_PARAMS.showViral)).thenReturn(ERROR_DATA)

                    useCase.execute(TEST_PARAMS) shouldBe ERROR_DATA
                }
            }
        }
    }

    companion object {
        private val TEST_PARAMS = Params(Section.user, Sort.top, 2, true)

        private val JPG_ITEM = GalleryItem(id = "lDRB2",
                                           title = "Imgur Office",
                                           description = null,
                                           datetime = 1357856292,
                                           cover = "24nLu",
                                           accountUrl = "Alan",
                                           accountId = 4,
                                           privacy = "public",
                                           layout = "blog",
                                           views = 13780,
                                           link = "http://alanbox.imgur.com/a/lDRB2",
                                           ups = 1602,
                                           downs = 14,
                                           points = 1588,
                                           score = 1917,
                                           isAlbum = true,
                                           vote = null,
                                           commentCount = 10,
                                           imagesCount = 11,
                                           images = emptyList(),
                                           animated = false,
                                           section = "section",
                                           bandwidth = 123L,
                                           height = 123,
                                           size = 123,
                                           type = "image/jpeg",
                                           width = 123)

        private val PNG_ITEM = GalleryItem(id = "lDRB2",
                                           title = "Imgur Office",
                                           description = null,
                                           datetime = 1357856292,
                                           cover = "24nLu",
                                           accountUrl = "Alan",
                                           accountId = 4,
                                           privacy = "public",
                                           layout = "blog",
                                           views = 13780,
                                           link = "http://alanbox.imgur.com/a/lDRB2",
                                           ups = 1602,
                                           downs = 14,
                                           points = 1588,
                                           score = 1917,
                                           isAlbum = true,
                                           vote = null,
                                           commentCount = 10,
                                           imagesCount = 11,
                                           images = emptyList(),
                                           animated = false,
                                           section = "section",
                                           bandwidth = 123L,
                                           height = 123,
                                           size = 123,
                                           type = "image/png",
                                           width = 123)

        private val GIF_ITEM = GalleryItem(id = "lDRB2",
                                           title = "Imgur Office",
                                           description = null,
                                           datetime = 1357856292,
                                           cover = "24nLu",
                                           accountUrl = "Alan",
                                           accountId = 4,
                                           privacy = "public",
                                           layout = "blog",
                                           views = 13780,
                                           link = "http://alanbox.imgur.com/a/lDRB2",
                                           ups = 1602,
                                           downs = 14,
                                           points = 1588,
                                           score = 1917,
                                           isAlbum = true,
                                           vote = null,
                                           commentCount = 10,
                                           imagesCount = 11,
                                           images = emptyList(),
                                           animated = false,
                                           section = "section",
                                           bandwidth = 123L,
                                           height = 123,
                                           size = 123,
                                           type = "image/gif",
                                           width = 123)

        private val ERROR_DATA = Result.Error(IllegalArgumentException("Some error"))
    }
}